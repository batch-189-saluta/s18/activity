console.log('Hello World')

function addition(firstNumber, secondNumber){
	let sum = 0
	sum = firstNumber + secondNumber
	console.log(`Displayed sum of ${firstNumber} and ${secondNumber}`)
	console.log(sum)
}
addition(5, 15)

function subtraction(thirdNumber, fourthNumber){
	let difference = 0
	difference = thirdNumber - fourthNumber
	console.log(`Displayed difference of ${thirdNumber} and ${fourthNumber}`)
	console.log(difference)
}
subtraction(20, 5)



function multiplication(fifthNumber, sixthNumber){
	let answerOne = 0
	answerOne = fifthNumber * sixthNumber
	console.log(`The product of ${fifthNumber} and ${sixthNumber}:`)
	return answerOne
}

let product = multiplication(50, 10)
console.log(product)

function division(seventhNumber, eightNumber){
	let answerTwo = 0
	answerTwo = seventhNumber / eightNumber
	console.log(`The quotient of ${seventhNumber} and ${eightNumber}:`)
	return answerTwo
}
let quotient = division(50,10)
console.log(quotient)

function circle(radius){
	const pi = 3.14;
	let areaCircle = 0
	areaCircle = pi * radius**2
	console.log(`The result of getting the area of a circle with ${radius}:`)
	return areaCircle
}
let circleArea = circle(15)
console.log(circleArea)


function average(num1, num2, num3, num4){
	let average = 0
	average = (num1+num2+num3+num4)/4
	console.log(`The average of ${num1}, ${num2}, ${num3} and ${num4}:`)
	return average
}
let averageVariable = average(20, 40, 60, 80)
console.log(averageVariable)

function passing(score, totalScore) {
	 let percentage = 0
	 percentage = (score / totalScore) * 100
	 isPassed = (percentage > 75)
	 console.log(`Is ${score}/${totalScore} a passing score?`)
	 return isPassed

}
let isPassingScore = passing(38,50)
console.log(isPassingScore)


